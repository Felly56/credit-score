package com.felly.creditscore.di

import com.felly.creditscore.core.Constants
import com.felly.creditscore.data.remote.api.CreditScoreApi
import com.felly.creditscore.domain.repository.MainRepositoryImpl
import com.felly.creditscore.presentation.repository.MainRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideGsonBuilder() : Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    @Singleton
    @Provides
    fun provideRetrofitInstance(gson: Gson, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Singleton
    @Provides
    fun provideCreditScoreApi(retrofit: Retrofit): CreditScoreApi = retrofit.create(CreditScoreApi::class.java)

    @Singleton
    @Provides
    fun provideManRepository(creditScoreApi: CreditScoreApi): MainRepository{
        return MainRepositoryImpl(creditScoreApi)
    }
}