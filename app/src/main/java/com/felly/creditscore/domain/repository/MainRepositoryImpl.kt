package com.felly.creditscore.domain.repository

import com.felly.creditscore.data.remote.api.CreditScoreApi
import com.felly.creditscore.data.remote.model.CreditScoreResponse
import com.felly.creditscore.presentation.repository.MainRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val creditScoreApi: CreditScoreApi
) : MainRepository {

    override fun getCreditScore(onSuccess: (CreditScoreResponse?) -> Unit, onError: (Throwable) -> Unit) {
        val response = creditScoreApi.getCreditScore()
        response.enqueue(object : Callback<CreditScoreResponse> {
            override fun onResponse(call: Call<CreditScoreResponse>, response: Response<CreditScoreResponse>) {
                if (response.isSuccessful && response.body() != null) {
                    onSuccess.invoke(response.body())
                } else {
                    onError.invoke(Throwable("Something went wrong"))
                }
            }

            override fun onFailure(call: Call<CreditScoreResponse>, t: Throwable) {
                onError.invoke(t)
            }
        })
    }
}