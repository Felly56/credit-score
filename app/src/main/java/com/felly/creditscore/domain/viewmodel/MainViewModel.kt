package com.felly.creditscore.domain.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.felly.creditscore.core.NetworkRequestState
import com.felly.creditscore.data.local.ui.CreditScoreUiData
import com.felly.creditscore.data.remote.model.CreditScoreResponse
import com.felly.creditscore.presentation.repository.MainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : ViewModel() {
    private val creditScoreRequestMutableStateFlow = MutableStateFlow<NetworkRequestState<CreditScoreUiData>>(NetworkRequestState.Idle)
    val creditScoreRequestStateFlow = creditScoreRequestMutableStateFlow.asStateFlow()

    lateinit var retrievedCreditScoreUiData: CreditScoreUiData

    fun getCreditScore() = viewModelScope.launch {
        creditScoreRequestMutableStateFlow.value = NetworkRequestState.Loading("Loading, please wait...")
        mainRepository.getCreditScore(onSuccess = {
            retrievedCreditScoreUiData = mapCreditScoreResponseToCreditScoreUiData(it!!)
            creditScoreRequestMutableStateFlow.value = NetworkRequestState.Success(retrievedCreditScoreUiData)
        }, onError = {
            creditScoreRequestMutableStateFlow.value = NetworkRequestState.Error(it, "${it.message}")
        })
    }

    private fun mapCreditScoreResponseToCreditScoreUiData(response: CreditScoreResponse): CreditScoreUiData{
        val score = response.creditReportInfo.score
        val maxScore = response.creditReportInfo.maxScoreValue
        return CreditScoreUiData(
            score,
            maxScore,
            response.creditReportInfo.status,
            getProgress(score, maxScore),
            response.creditReportInfo.clientRef,
            response.dashboardStatus,
            response.creditReportInfo.percentageCreditUsed,
            response.creditReportInfo.currentShortTermDebt,
            response.creditReportInfo.currentShortTermCreditLimit,
            response.creditReportInfo.currentLongTermCreditLimit
        )
    }

    private fun getProgress(score: Int, maxScore: Int): Int{
        return (score * 100) / maxScore
    }
}