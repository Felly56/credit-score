package com.felly.creditscore.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.felly.creditscore.R
import com.felly.creditscore.databinding.CreditScoreDetailsFragmentBinding
import com.felly.creditscore.domain.viewmodel.MainViewModel

class CreditScoreDetailsFragment : Fragment(R.layout.credit_score_details_fragment) {
    private var _binding: CreditScoreDetailsFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = CreditScoreDetailsFragmentBinding.bind(view)
        updateViews()
    }

    private fun updateViews() = with(binding){
        val creditScoreUiData = viewModel.retrievedCreditScoreUiData
        clientRef.text = getString(R.string.client_reference, creditScoreUiData.clientRef)
        dashboardStatusTextView.text = getString(R.string.client_reference, creditScoreUiData.dashboardStatus)
        statusTextView.text = creditScoreUiData.status
        percentageCreditUsedTextView.text = getString(R.string.percentage_credit_used, creditScoreUiData.percentageCreditUsed)
        currentShortTermDebtTextView.text = getString(R.string.current_short_term_debt, creditScoreUiData.currentShortTermDebt)
        currentShortTermCreditLimitTextView.text = getString(R.string.current_short_term_credit_limit, creditScoreUiData.currentShortTermCreditLimit)
        currentLongTermDebtTextView.text = getString(R.string.current_long_term_debt, creditScoreUiData.currentLongTermCreditLimit)
    }
}