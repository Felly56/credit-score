package com.felly.creditscore.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.felly.creditscore.R
import com.felly.creditscore.core.NetworkRequestState
import com.felly.creditscore.data.local.ui.CreditScoreUiData
import com.felly.creditscore.databinding.HomeFragmentBinding
import com.felly.creditscore.domain.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.home_fragment) {

    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        _binding = HomeFragmentBinding.bind(view)
        viewModel.getCreditScore()
        observeGetCreditScoreRequest()

        binding.retryButton.setOnClickListener {
            viewModel.getCreditScore()
        }

        binding.creditScoreParentView.setOnClickListener {
            (requireActivity() as MainActivity).doFragmentTransaction(CreditScoreDetailsFragment())
        }
    }

    private fun observeGetCreditScoreRequest() {
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.creditScoreRequestStateFlow.collectLatest {
                    handleGetCreditScoreRequestState(it)
                }
            }
        }
    }

    private fun handleGetCreditScoreRequestState(creditScoreRequestState: NetworkRequestState<CreditScoreUiData>) = with(binding) {
        when (creditScoreRequestState) {
            is NetworkRequestState.Loading -> {
                creditScoreParentView.visibility = View.INVISIBLE
                loaderParentView.visibility = View.VISIBLE
                errorParentView.visibility = View.GONE
            }
            is NetworkRequestState.Error -> {
                creditScoreParentView.visibility = View.INVISIBLE
                loaderParentView.visibility = View.GONE
                errorParentView.visibility = View.VISIBLE
                errorTextView.text = creditScoreRequestState.message
            }
            is NetworkRequestState.Success -> {
                creditScoreParentView.visibility = View.VISIBLE
                loaderParentView.visibility = View.GONE
                errorParentView.visibility = View.GONE
                progressTextView.text = creditScoreRequestState.data.score.toString()
                maxCreditScoreTextView.text = getString(R.string.out_of, creditScoreRequestState.data.maxScore)
                creditScoreProgress.progress = creditScoreRequestState.data.progress
            }
            else -> {}
        }
    }

    override fun onDestroy() {
        _binding = null
        super.onDestroy()
    }
}