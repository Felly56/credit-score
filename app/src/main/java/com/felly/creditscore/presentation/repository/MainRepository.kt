package com.felly.creditscore.presentation.repository

import com.felly.creditscore.data.remote.model.CreditScoreResponse

interface MainRepository {
    fun getCreditScore(onSuccess: (CreditScoreResponse?) -> Unit, onError: (Throwable) -> Unit)
}