package com.felly.creditscore.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CreditScoreInfoResponse(
    @SerializedName("score")
    @Expose
    val score: Int,

    @SerializedName("scoreBand")
    @Expose
    val scoreBand: Int,

    @SerializedName("status")
    @Expose
    val status: String,

    @SerializedName("maxScoreValue")
    @Expose
    val maxScoreValue: Int,

    @SerializedName("clientRef")
    @Expose
    val clientRef: String,

    @SerializedName("percentageCreditUsed")
    @Expose
    val percentageCreditUsed: Int,

    @SerializedName("currentShortTermDebt")
    @Expose
    val currentShortTermDebt: Int,

    @SerializedName("currentShortTermCreditLimit")
    @Expose
    val currentShortTermCreditLimit: Int,

    @SerializedName("currentLongTermCreditLimit")
    @Expose
    val currentLongTermCreditLimit: Int
)
