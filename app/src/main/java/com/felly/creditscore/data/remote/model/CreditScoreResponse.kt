package com.felly.creditscore.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CreditScoreResponse(
    @SerializedName("accountIDVStatus")
    @Expose
    val accountIDVStatus: String,
    @SerializedName("creditReportInfo")
    @Expose
    val creditReportInfo: CreditScoreInfoResponse,

    @SerializedName("dashboardStatus")
    @Expose
    val dashboardStatus: String,

)
