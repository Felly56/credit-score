package com.felly.creditscore.data.local.ui

data class CreditScoreUiData(
    val score: Int,
    val maxScore: Int,
    val status: String,
    val progress: Int,
    val clientRef: String,
    val dashboardStatus: String,
    val percentageCreditUsed: Int,
    val currentShortTermDebt: Int,
    val currentShortTermCreditLimit: Int,
    val currentLongTermCreditLimit: Int
)