package com.felly.creditscore.data.remote.api

import com.felly.creditscore.data.remote.model.CreditScoreResponse
import retrofit2.Call
import retrofit2.http.GET

interface CreditScoreApi {
    @GET("endpoint.json")
    fun getCreditScore() : Call<CreditScoreResponse>
}