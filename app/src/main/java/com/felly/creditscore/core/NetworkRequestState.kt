package com.felly.creditscore.core

sealed class NetworkRequestState<out R>{
    object Idle: NetworkRequestState<Nothing>()
    data class Loading(val loadingMessage: String): NetworkRequestState<Nothing>()
    data class Error(val exception: Throwable, val message: String) : NetworkRequestState<Nothing>()
    data class Success<out T>(val data: T) : NetworkRequestState<T>()
}
